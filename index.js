

const { SerialPort } = require("serialport");
const { ReadlineParser } = require("@serialport/parser-readline");
const port = new SerialPort({ path: "/dev/ttyUSB0", baudRate: 115200 });

const parser = port.pipe(new ReadlineParser({ delimiter: "\r\n" }));

// Add a listener for data received from the serial port
parser.on("data", (data) => {
  console.log("Received:", data);
});

// // Function to send data to the serial port
// function sendData(dataToSend) {
//   port.write(dataToSend, (error) => {
//     if (error) {
//       console.error('Error sending data:', error);
//     } else {
//       console.log('Data sent:', dataToSend);
//     }
//   });
// }

function sendJsonData(data) {
  const jsonData = JSON.stringify(data);
  port.write(jsonData, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonData);
    }
  });
}

const jsonToSend = {
  max_vol_A: 2,
  max_vol_B: 42,
  interval_vol: 30,
  calibration: 20,
};
sendJsonData(jsonToSend);

// Example usage: Send data and wait for a response
//sendData('Hello, Serial Port!\r\n');

// You can send more data and listen for responses as needed
