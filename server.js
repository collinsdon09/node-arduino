

var express = require("express");
var app = express();
var http = require("http").createServer(app);
var socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:3000",
  },
});


const { SerialPort } = require("serialport");
const { ReadlineParser } = require("@serialport/parser-readline");
const port = new SerialPort({ path: "/dev/ttyUSB0", baudRate: 9600 });
const parser = port.pipe(new ReadlineParser({ delimiter: "\r\n" }));


function sendJsonData(data) {
  const jsonData = JSON.stringify(data);
  port.write(jsonData, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonData);
    }
  });
}




parser.on("data", (data) => {
  console.log("Received:", data);

  //console.log(typeof data)

  try {
    const jsonObject = JSON.parse(data);
    console.log(jsonObject);

  console.log(jsonObject.status)
  } catch (error) {
    console.error('Error parsing JSON:', error);
  }
});




var connectedClients = {}; // Track connected clients

http.listen(4000, function () {
  console.log("Server is started....");

  socketIO.on("connection", function (socket) {
    console.log("New client connected:", socket.id);

    //socket.emit("receipt_event", new Date(Date.now())); // Send initial event to the connected client
    socket.on('chat-message', (data) => {
      //console.log(data);
      const jsonToSend = {
        status:"configuration",
        max_vol_A: data.text,
        max_vol_B: 0,
        interval_vol: 0,
        calibration: 0,
      };
      sendJsonData(jsonToSend);

      // Handle the incoming message here as needed
      // You can also emit messages to other connected clients, broadcast, etc.
      // For example, to broadcast the message to all connected clients:
      socketIO.emit('chat-message', data);
    });
 

    socket.on("disconnect", function () {
      //clearInterval(intervalId); // Stop sending events when client disconnects
      delete connectedClients[socket.id];
      //console.log("Client disconnected:", socket.id);
    });
  });
});