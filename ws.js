var express = require("express");
var app = express();
var http = require("http").createServer(app);
var socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:5173",
  },
});

var receipt = {
  receipt_id: "8-98752",
  time: "14:20",
  Client: "John Wick",
  Waiter: "Tom",
  Item_number: "13",
  timestamp: new Date().getTime(),
};

var rec = {
  receipt_number: new Date().getTime(),
  note: "Sample note",
 
};

var connectedClients = {}; // Track connected clients

http.listen(4000, function () {
  console.log("Server is started....");

  socketIO.on("connection", function (socket) {
    console.log("New client connected:", socket.id);

    //socket.emit("receipt_event", new Date(Date.now())); // Send initial event to the connected client

    var intervalId = setInterval(function () {
      var receipt = {
        receipt_id: "8-98752",
        time: "14:20",
        Client: "John Wick",
        Waiter: "Tom",
        Item_number: "13",
        timestamp: new Date(Date.now()),
      };


var rec = {
  receipt_number: new Date().getTime(),
  note: "Sample note",
  receipt_type: "Sales",
  refund_for: null,
  status:"not_picked"
 
};


      socket.emit("receipt_event", rec); // Send event to the connected client every 10 seconds
    }, 1000);

    socket.on("disconnect", function () {
      clearInterval(intervalId); // Stop sending events when client disconnects
      delete connectedClients[socket.id];
      //console.log("Client disconnected:", socket.id);
    });
  });
});