var express = require("express");
var app = express();
var http = require("http").createServer(app);
var socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:3000",
  },
});

const { SerialPort } = require("serialport");
const { ReadlineParser } = require("@serialport/parser-readline");
const sqlite3 = require("sqlite3").verbose(); // Import the sqlite3 package
const db = new sqlite3.Database("mydb.sqlite"); // Create or open a SQLite database file

// Create a table to store data if it doesn't exist
db.serialize(function () {
  db.run(
    "CREATE TABLE IF NOT EXISTS sensor_data (id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)"
  );
});

const port = new SerialPort({ path: "/dev/ttyUSB0", baudRate: 9600 });
const parser = port.pipe(new ReadlineParser({ delimiter: "\r\n" }));

function pJson(data) {
  try {
    const jsonObject = JSON.parse(data);
    // console.log(jsonObject);

    //console.log(jsonObject);
    return jsonObject;
  } catch (error) {
    console.error("Error parsing JSON:", error);
  }
}

function sendJsonData(data) {
  // Example usage:
  extractData((err, data) => {
    if (err) {
      console.error("Error:", err);
    } else {
      console.log("Extracted Data:", data);
      const lastJsonString = data[data.length - 1];

      console.log("last data", lastJsonString);

      console.log(pJson(lastJsonString));
      calibration = pJson(lastJsonString);

      //console.log(calibration.volume)

      const data_to_send = {
        status: "calibration",
        max_vol_A: data.text,
        max_vol_B: 0,
        interval_vol: 0,
        calibration: 0,
        actual_measurement: calibration.volume,
      };

      const jsonToSend = JSON.stringify(data_to_send);

      port.write(jsonToSend, (error) => {
        if (error) {
          console.error("Error sending JSON data:", error);
        } else {
          console.log("JSON data sent:", jsonToSend);
        }
      });
    }
  });
  // const jsonData = JSON.stringify(data);
  // port.write(jsonData, (error) => {
  //   if (error) {
  //     console.error("Error sending JSON data:", error);
  //   } else {
  //     console.log("JSON data sent:", jsonData);
  //   }
  // });
}
function sendMeasureStatus(data) {
  console.log("measure?", data.start);
  
  const data_to_send = {
    status: data.start,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function extractData(callback) {
  const query = "SELECT * FROM sensor_data";

  db.all(query, (err, rows) => {
    if (err) {
      console.error("Error extracting data from the database:", err);
      callback(err, null);
    } else {
      // Extracted data is an array of objects, each containing a "data" field
      const extractedData = rows.map((row) => row.data);
      callback(null, extractedData);
    }
  });
}

parser.on("data", (data) => {
  console.log("Received:", data);

  try {
    const jsonObject = JSON.parse(data);
    console.log(jsonObject);

    //console.log(jsonObject.status);
  } catch (error) {
    console.error("Error parsing JSON:", error);
  }
});

var connectedClients = {}; // Track connected clients

http.listen(4000, function () {
  console.log("Server is started....");

  socketIO.on("connection", function (socket) {
    console.log("New client connected:", socket.id);

    socket.on("measure", (data) => {
      
      sendMeasureStatus(data);
    });

    socket.on("save", (data) => {
      const jsonToInsert = JSON.stringify(data); // Convert the data object to a JSON string

      // console.log("data", data);

      db.run(
        "INSERT OR REPLACE INTO sensor_data (data) VALUES (?)",
        [jsonToInsert],
        function (err) {
          if (err) {
            console.error("Error inserting data into the database:", err);
          } else {
            console.log("Data replaced in the database.");
          }
        }
      );

      // After replacing data, you can send it to the serial port
      sendJsonData(data);
    });

    socket.on("disconnect", function () {
      //clearInterval(intervalId); // Stop sending events when client disconnects
      delete connectedClients[socket.id];
      //console.log("Client disconnected:", socket.id);
    });
  });
});
